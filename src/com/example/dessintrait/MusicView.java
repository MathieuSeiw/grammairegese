package com.example.dessintrait;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class MusicView extends View{

	public MusicView(Context context) {
		super(context);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		
		canvas.drawCircle(100, 100, 100, new Paint());
	}

}
