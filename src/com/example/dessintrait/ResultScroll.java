package com.example.dessintrait;

/**
 * Permet de stocker les r�sultats de l'analyse d'un scroll
 * @author Axel
 *
 */
public class ResultScroll {
	
	private boolean scroll = false;
	private int departX;
	private int departY;
	private int finX;
	private int finY;
	private int maxEcartX;
	private int maxEcartY;
	
	/**
	 * Ecart max entre le d�part et la gauche
	 */
	private int maxDispersionG;
	
	/**
	 * Ecart max entre le d�part et la droite
	 */
	private int maxDispersionD;
	
	/**
	 * Nombre maximum de doigts pos� durant toute la dur�e du scroll
	 */
	private int maxDoigts = 1;
	
	/**
	 * Exemple 10 event scroll et les 3 doigts pendant seulement 5 event alors on aura 50%
	 */
	private int pourcentageMaxDoigts = 0;
	
	public ResultScroll() {
		super();
	}

	public int getDepartX() {
		return departX;
	}

	public void setDepartX(int departX) {
		this.departX = departX;
	}

	public int getDepartY() {
		return departY;
	}

	public void setDepartY(int departY) {
		this.departY = departY;
	}

	public int getFinX() {
		return finX;
	}

	public void setFinX(int finX) {
		this.finX = finX;
	}

	public int getFinY() {
		return finY;
	}

	public void setFinY(int finY) {
		this.finY = finY;
	}

	public int getMaxEcartX() {
		return maxEcartX;
	}

	public void setMaxEcartX(int maxEcartX) {
		this.maxEcartX = maxEcartX;
	}

	public int getMaxEcartY() {
		return maxEcartY;
	}

	public void setMaxEcartY(int maxEcartY) {
		this.maxEcartY = maxEcartY;
	}

	public int getMaxDispersionG() {
		return maxDispersionG;
	}

	public void setMaxDispersionG(int maxDispersionG) {
		this.maxDispersionG = maxDispersionG;
	}

	public int getMaxDispersionD() {
		return maxDispersionD;
	}

	public void setMaxDispersionD(int maxDispersionD) {
		this.maxDispersionD = maxDispersionD;
	}

	public int getMaxDoigts() {
		return maxDoigts;
	}

	public void setMaxDoigts(int maxDoigts) {
		this.maxDoigts = maxDoigts;
	}

	public int getPourcentageMaxDoigts() {
		return pourcentageMaxDoigts;
	}

	public void setPourcentageMaxDoigts(int pourcentageMaxDoigts) {
		this.pourcentageMaxDoigts = pourcentageMaxDoigts;
	}

	public boolean hasScroll() {
		return scroll;
	}

	public void setScroll(boolean scroll) {
		this.scroll = scroll;
	}
}
