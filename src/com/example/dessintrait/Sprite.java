package com.example.dessintrait;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Sprite {
	protected float mPosX;
	protected float mPosY;
	protected Bitmap _Bmp;
    
    public Sprite(Bitmap bmp,float posX,float posY) {
    	mPosX = posX;
    	mPosY = posY;
        this._Bmp=bmp;
    }
    
    public void updateWithPhysics() {
    	
    	
    }
    
	public float getmPosX() {
		return mPosX;
	}

	public void setmPosX(float mPosX) {
		this.mPosX = mPosX;
	}

	public float getmPosY() {
		return mPosY;
	}

	public void setmPosY(float mPosY) {
		this.mPosY = mPosY;
	}
}
