package com.example.dessintrait;

import java.util.List;

import android.graphics.Point;

public class EventGestePress extends EventGesteTap {
	
	EventGestePress(List<Point> p) {
		super(p);
	}
	
	@Override
	public int getType() {
		return EVENT_PRESS;
	}

}
