package com.example.dessintrait;

import java.util.List;

import android.util.Log;

public class corDetectionCroche extends corDetectionGeste {

	@Override
	void createGeste(List<EventGeste> listeEventGeste) {
		if(listeEventGeste == null)
			return;
		
		Log.d("test","debug croche " + listeEventGeste.size());
		
		if(listeEventGeste.size() > 1) //Si y'a qu'un �l�ment
		{
			ResultScroll rs = analyseScroll(listeEventGeste);
			
			Log.d("test","ecart :" + rs.getMaxEcartY());
			
			if(rs.getMaxEcartY()!=0 && rs.getMaxEcartY() > (ScrollTest.sizeY*0.1) && rs.getMaxDoigts() == 1)
			{
				Log.d("test","Croche detecte !");
			}	
		}
		
		if(get_Next() != null)
			get_Next().createGeste(listeEventGeste);
		else
			return;
	}

}
