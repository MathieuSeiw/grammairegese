package com.example.dessintrait;

import java.util.List;

import android.graphics.Point;

public class EventGesteLongTap extends EventGesteTap {
	
	EventGesteLongTap(List<Point> p) {
		super(p);
	}

	@Override
	public int getType() {
		return EVENT_LONGTAP;
	}

}
