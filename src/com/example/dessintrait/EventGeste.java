package com.example.dessintrait;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Point;

/*
 * Interface pour les types de gestes, ils ont en communs d'avoir un type pour pouvoir �tre trait� dans le COR apr�s
 */
public abstract class EventGeste {
	
	public static int EVENT_DOUBLETAP = 1;
	public static int EVENT_LONGTAP = 2;
	public static int EVENT_SHORTTAP = 3;
	public static int EVENT_TRANSLATE = 4;
	public static int EVENT_PRESS = 5;
	
	/**
	 * Liste des coordon�es de chaque doigts
	 */
	protected List<Point> points  = new ArrayList<Point>();
	
	EventGeste(List<Point> p){
		points = p;
	}
	
	protected int _NbPoint = 0;
	
	abstract int getType();
	
	public int getNbDoigts()
	{
		return points.size();
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> point) {
		this.points = point;
	}

}
