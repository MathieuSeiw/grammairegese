package com.example.dessintrait;

import java.util.List;

import android.util.Log;

abstract class corDetectionGeste {
	
	corDetectionGeste _Next;
	
	void set_Next(corDetectionGeste next){
		_Next = next;
	}
	corDetectionGeste get_Next(){
		return _Next;
	}
	
	/*
	 * A voir ce qu'il renvoie
	 */
	abstract void createGeste(List<EventGeste> listeEventGeste);
	
	public ResultScroll analyseScroll(List<EventGeste> listeEventGeste)
	{
		ResultScroll rs = new ResultScroll();
		
		int i;
		
		int departy = 0;
		int departx = 0;
		int maxy = listeEventGeste.get(0).getPoints().get(0).y;;
		int maxx = listeEventGeste.get(0).getPoints().get(0).x;;
		int ecartTotal;
		int nbMaxDoigts = 1;
		
		// nb fois qu'apparait le doigts maximum
		int compteurMaxDoigt = 1;
		
		boolean first = true;
		boolean scroll = false;
		
		for(i=0;i<listeEventGeste.size();i++)
		{
			if(listeEventGeste.get(i).getType()==EventGeste.EVENT_TRANSLATE)
			{	
				scroll = true;
				if(listeEventGeste.get(i).getPoints().size() > nbMaxDoigts) 
				{
					nbMaxDoigts =listeEventGeste.get(i).getPoints().size();
					compteurMaxDoigt = 1;
				}
				else
					compteurMaxDoigt++;
					
				int y = listeEventGeste.get(i).getPoints().get(0).y;
				int x = listeEventGeste.get(i).getPoints().get(0).x;
				
				if(i==0)
				{
					departy = y;
				}
				
	
				if(first)
				{
					departy = listeEventGeste.get(i).getPoints().get(0).y;
					departx = listeEventGeste.get(i).getPoints().get(0).x;
					maxy = listeEventGeste.get(i).getPoints().get(0).y;
					first = false;
					rs.setDepartX(departx);
					rs.setDepartY(departy);
				}
				
				if(y < maxy) maxy = y;
				if(x > maxx) maxx = x;
			}
			
		}
		
		ecartTotal = departy - maxy;
		
		float ratioDoigt = (float)compteurMaxDoigt/(float)listeEventGeste.size();
		
		Log.i("test","ratio : " + ratioDoigt);
		rs.setMaxEcartY(ecartTotal);
		rs.setMaxDoigts(nbMaxDoigts);
		Log.i("test","compteur max duree doigt : " + compteurMaxDoigt);
		rs.setPourcentageMaxDoigts((int)(ratioDoigt*100));
		rs.setScroll(scroll);
		rs.setMaxEcartX(departx - maxx);
		
		return rs;
	}
}
