package com.example.dessintrait;

import java.util.List;

import android.graphics.Point;

public class EventGesteDoubleTap extends EventGesteTap {

	EventGesteDoubleTap(List<Point> p) {
		super(p);
	}

	@Override
	public int getType() {
		return EVENT_DOUBLETAP;
	}

}
