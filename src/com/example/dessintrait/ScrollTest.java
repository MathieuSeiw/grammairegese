package com.example.dessintrait;

import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScrollTest extends Activity implements OnGestureListener, GestureDetector.OnDoubleTapListener {    
    private LinearLayout main;    
    private TextView viewA;
    private List<EventGeste> listEvent = new ArrayList<EventGeste>();
    
    public static int naturalOrientation = 0;
    public static int sizeX = 0;
    public static int sizeY = 0;
    
    // D�claration de la chaine de r�sponsabilit�
	corDetectionGeste chain = new corDetectionBlanche();
	 
	corDetectionGeste detectDC = new corDetectionDoubleCroche();
	corDetectionGeste detectNoir = new corDetectionNoir();
	corDetectionGeste detectCroche = new corDetectionCroche();
	corDetectionGeste detectPortee = new corDetectionPortee();
    
    private GestureDetector gestureScanner;
        
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          
        gestureScanner = new GestureDetector(this);
          
        main = new LinearLayout(this);
        main.setBackgroundColor(Color.GRAY);
        main.setLayoutParams(new LinearLayout.LayoutParams(320,480));
          
        viewA = new TextView(this);
        viewA.setBackgroundColor(Color.YELLOW);
        viewA.setTextColor(Color.BLACK);
        viewA.setTextSize(16);
        viewA.setLayoutParams(new LinearLayout.LayoutParams(320,80));
        main.addView(viewA);
        
    	detectNoir.set_Next(detectCroche);
    	detectDC.set_Next(detectNoir);
    	detectPortee.set_Next(detectDC);
    	
    	chain.set_Next(detectPortee);
        
    	//Portrait : 1
    	//Landscape : 2
    	naturalOrientation = getDeviceDefaultOrientation();
    	Log.i("test","orientation : " + getDeviceDefaultOrientation());
    	
    	Display display = getWindowManager().getDefaultDisplay();
    	Point size = new Point();
    	display.getSize(size);
    	sizeX = size.x;
    	sizeY = size.y;
    	
    	Log.i("test","sizeX : " + sizeX + " | sizeY : " + sizeY);
        //setContentView(main);
    	
        setContentView(new MusicView(this));
    }
   
    @Override
    public boolean onTouchEvent(MotionEvent me) {
    	int action = me.getAction();

	    switch (action) {
	    case MotionEvent.ACTION_DOWN:
	    	//On est dans un nouveau geste => drop de la liste
			this.listEvent.clear();
			break;

	    case MotionEvent.ACTION_MOVE:
	    	//On bouge, rien � faire
	        break;

	    case MotionEvent.ACTION_UP:
	    	//Geste fini
	    	//Affichage
	    	Log.d("test","list event : "+ listEvent.toString());
	    	//On reconnait le geste
	    	//Cor de geste qui a les donn�es m�tiers
	    	chain.createGeste(listEvent);
	    	//Action � r�aliser avec le geste
	    	break;
	    default:
	    	Log.d("test", "Action Inconnu ");
	      break;
	    }
	    //Catch des events
        return gestureScanner.onTouchEvent(me);
    }
   
    @Override
    public boolean onDown(MotionEvent e) {
        Log.d("test","-" + "onDown" + "-");
        return true;
    }
    
   
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
    	//Appell� parfois a la fin de
        Log.d("test","-" + "onFling" + "-" + e1.getPointerCount() + "-" + e2.getPointerCount());
        return true;
    }
   
    @Override
    public void onLongPress(MotionEvent e) {
        Log.d("test","-" + "clic long" + "-" + e.getPointerCount());
        List<Point> lp = new ArrayList<Point>();
        
        for(int i = 0;i<e.getPointerCount();i++)
        {
        	lp.add(new Point((int)e.getX(i),(int)e.getY(i)));
        }
        this.listEvent.add(new EventGesteLongTap(lp));
    }
   
    /*
     * (non-Javadoc)
     * @see android.view.GestureDetector.OnGestureListener#onScroll(android.view.MotionEvent, android.view.MotionEvent, float, float)
     * Translate
     */
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        List<Point> lp = new ArrayList<Point>();
        
        for(int i = 0;i<e2.getPointerCount();i++)
        {
        	lp.add(new Point((int)e2.getX(i),(int)e2.getY(i)));
        }
        
        Log.i("test","nb doigts :" + e2.getPointerCount() );
        Log.i("test","arrive x : " + (int)e2.getX() + " | y : " + (int)e2.getY() );
        this.listEvent.add(new EventGesteTranslate(lp));
        return true;
    }
   
    /*
     * (non-Javadoc)
     * @see android.view.GestureDetector.OnGestureListener#onShowPress(android.view.MotionEvent)
     * Permet de savoir quand il laisse le doigt appuyer
     */
    @Override
    public void onShowPress(MotionEvent e) {
        Log.d("test","-" + "onShowPress" + "-");
        List<Point> lp = new ArrayList<Point>();
        
        for(int i = 0;i<e.getPointerCount();i++)
        {
        	lp.add(new Point((int)e.getX(i),(int)e.getY(i)));
        }
        
        this.listEvent.add(new EventGestePress(lp));
    }    
   
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
    	//Useless, y'a le confirm
        Log.d("test","-" + "simple touch" + "-" + e.getPointerCount());
        return true;
    }

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		Log.d("test","-" + "onSingleTapConfirmed" + "-" + e.getPointerCount());
        List<Point> lp = new ArrayList<Point>();
        
        for(int i = 0;i<e.getPointerCount();i++)
        {
        	lp.add(new Point((int)e.getX(i),(int)e.getY(i)));
        }
        
		this.listEvent.add(new EventGesteShortTap(lp));
		chain.createGeste(listEvent);
		return false;
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		Log.d("test","-" + "onDoubleTap" + "-" + e.getPointerCount());
        List<Point> lp = new ArrayList<Point>();
        
        for(int i = 0;i<e.getPointerCount();i++)
        {
        	lp.add(new Point((int)e.getX(i),(int)e.getY(i)));
        }
        
		this.listEvent.add(new EventGesteDoubleTap(lp));
		return false;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		//C'est appell� toujours ensemble ? OnDoubleTap + onDoubleTapEvent
		Log.d("test","-" + "onDoubleTapEvent" + "-" + e.getPointerCount());
		return false;
	}
	
	public int getDeviceDefaultOrientation() {

	    WindowManager windowManager =  (WindowManager) getSystemService(WINDOW_SERVICE);

	    Configuration config = getResources().getConfiguration();

	    int rotation = windowManager.getDefaultDisplay().getRotation();

	    if ( ((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) &&
	            config.orientation == Configuration.ORIENTATION_LANDSCAPE)
	        || ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) &&    
	            config.orientation == Configuration.ORIENTATION_PORTRAIT)) {
	      return Configuration.ORIENTATION_LANDSCAPE;
	    } else { 
	      return Configuration.ORIENTATION_PORTRAIT;
	    }
	}
} 