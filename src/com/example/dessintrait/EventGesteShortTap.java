package com.example.dessintrait;

import java.util.List;

import android.graphics.Point;

public class EventGesteShortTap extends EventGesteTap {

	EventGesteShortTap(List<Point> p) {
		super(p);
	}
	@Override
	public int getType() {
		return EVENT_SHORTTAP;
	}

}
