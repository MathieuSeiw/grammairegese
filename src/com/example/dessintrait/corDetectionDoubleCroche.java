package com.example.dessintrait;

import java.util.List;

import android.util.Log;

public class corDetectionDoubleCroche extends corDetectionGeste {

	@Override
	void createGeste(List<EventGeste> listeEventGeste) {
		if(listeEventGeste == null)
			return;
		Log.d("test","debug double croche " + listeEventGeste.size());
		
		if(listeEventGeste.size() == 1) //Si y'a qu'un �l�ment
		{
			if(listeEventGeste.get(0).getType() == EventGeste.EVENT_DOUBLETAP) //Que c'est doubleTap
			{
				if(listeEventGeste.get(0).getNbDoigts() == 1) //Qu'avec un doigt
				{
					//C'est un geste de simple croche ?
					//Test si on est bien � un endroit de simple croche
					Log.d("test","Double Croche detecte !");
					return;
				}
			}
		}
		
		if(get_Next() != null)
			get_Next().createGeste(listeEventGeste);
		else
			return;
	}

}
