package com.example.dessintrait;

import java.util.List;

import android.graphics.Point;

public abstract class EventGesteTap extends EventGeste {
	
	EventGesteTap(List<Point> p) {
		super(p);
	}

	@Override
	abstract public int getType();
}

