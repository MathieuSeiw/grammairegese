package com.example.dessintrait;

import java.util.List;

import android.util.Log;

public class corDetectionPortee extends corDetectionGeste {

	@Override
	void createGeste(List<EventGeste> listeEventGeste) {
		Log.d("test","debug portee " + listeEventGeste.size());
		// TODO Auto-generated method stub
		if(listeEventGeste == null)
			return;
		
		if(listeEventGeste.size() > 1) //Si y'a qu'un �l�ment
		{
			ResultScroll rs = analyseScroll(listeEventGeste);
			
			Log.d("test","max doigts  " + rs.getMaxDoigts());
			Log.d("test","%max doigts  " + rs.getPourcentageMaxDoigts());
			Log.d("test","distance x  " + rs.getMaxEcartX());
			Log.d("test","-ScrollTest.sizeX*0.8 x  " + -ScrollTest.sizeX*0.8);
			
			if(rs.getMaxDoigts()==3 && rs.getPourcentageMaxDoigts() > 50 && rs.getMaxEcartX() < -ScrollTest.sizeX*0.8)
			{
				Log.d("test","Portee detecte !");
				return;
			}
			
		}
		
		if(get_Next() != null)
			get_Next().createGeste(listeEventGeste);
		else
			return;
	}

}
