package com.example.dessintrait;

import java.util.List;

import android.graphics.Point;

public class EventGesteTranslate extends EventGeste {
	
	EventGesteTranslate(List<Point> p) {
		super(p);
	}
	@Override
	public int getType() {
		return EVENT_TRANSLATE;
	}

}
