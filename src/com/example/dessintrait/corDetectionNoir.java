package com.example.dessintrait;

import java.util.List;

import android.util.Log;

public class corDetectionNoir extends corDetectionGeste {

	@Override
	void createGeste(List<EventGeste> listeEventGeste) {
		if(listeEventGeste == null)
			return;
		
		Log.d("test","debug noir " + listeEventGeste.size());
		
		if(listeEventGeste.size() == 1) //Si y'a qu'un �l�ment
		{
			if(listeEventGeste.get(0).getType() == EventGeste.EVENT_SHORTTAP)
			{
				if(listeEventGeste.get(0).getNbDoigts() == 1) //Qu'avec un doigt
				{
					//C'est un geste de  blanche
					//Test si on est bien l� o� il faut
					Log.d("test","Noir detecte !");
					return;
				}
			}
		}
		
		if(get_Next() != null)
			get_Next().createGeste(listeEventGeste);
		else
			return;
	}

}
